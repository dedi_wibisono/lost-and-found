<?php echo ($header != NULL) ? $header : 'header not configured properly!'; ?>
					<li class="active"><?php echo anchor('admin', 'Found'); ?></li>
					<li><?php echo anchor('admin/lost', 'Lost'); ?></li>
					<li><?php echo anchor('admin/testimonials', 'Testimonials'); ?></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="well well-sm">
					<ul class="nav nav-pills nav-stacked">
						<li><?php echo anchor('admin', 'Found List'); ?></li>
						<li><?php echo anchor('admin/found_add', 'Add Found'); ?></li>
					</ul>
				</div>
				<?php echo ($sidebar != NULL) ? $sidebar : ''; ?>
			</div>
			<div class="col-md-9">
				<?php if($this->session->flashdata('error')) : ?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php endif; ?>
				<?php echo form_open_multipart('admin/found_edit_post', array('class' => 'form-horizontal', 'role' => 'form')); ?>
					<?php echo form_hidden('id', $found->id); ?>
					<div class="form-group">
						<?php echo form_label('Found Title', 'title', array('class' => 'col-md-2 control-label')); ?>
						<div class="col-md-10">
							<?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $found->title, 'placeholder' => 'Found Title')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo form_label('Found Content', 'content', array('class' => 'col-md-2 control-label')); ?>
						<div class="col-md-10">
							<?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'form-control', 'value' => $found->content, 'placeholder' => 'Found Content')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo form_label('Status', 'status', array('class' => 'col-md-2 control-label')); ?>
						<div class="col-md-10">
							<?php $options = array(
								'0'	=> 'Belum dikembalikan',
								'1' => 'Telah dikembalikan'
							);
							$js = 'id="status" class="form-control"';
							echo form_dropdown('status', $options, $found->status, $js); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo form_label('Image', 'image', array('class' => 'col-md-2 control-label')); ?>
						<div class="col-md-10">
							<?php echo form_upload(array('name' => 'image', 'id' => 'image', 'class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<?php echo form_submit(array('name' => 'submit', 'value' => 'Publish', 'class' => 'btn btn-primary')); ?>
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
<?php echo ($footer != NULL) ? $footer : 'footer not configured properly!'; ?>