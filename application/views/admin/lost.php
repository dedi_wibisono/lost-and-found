<?php echo ($header != NULL) ? $header : 'header not configured properly!'; ?>
					<li><?php echo anchor('admin', 'Found'); ?></li>
					<li class="active"><?php echo anchor('admin/lost', 'Lost'); ?></li>
					<li><?php echo anchor('admin/testimonials', 'Testimonials'); ?></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="well well-sm">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><?php echo anchor('admin/lost', 'Lost List'); ?></li>
						<li><?php echo anchor('admin/lost_add', 'Add Lost'); ?></li>
					</ul>
				</div>
				<?php echo ($sidebar != NULL) ? $sidebar : ''; ?>
			</div>
			<div class="col-md-9">
			<?php if($this->session->flashdata('succeess')) : ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php endif; ?>
			<?php foreach($lost_list->result() as $lost) : ?>
				<div class="well well-sm">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">
								<?php echo anchor('admin/lost_details/' . $lost->id, $lost->title); ?> 
								<small><?php echo anchor('admin/lost_delete/' . $lost->id, 'Delete', array('class' => 'btn btn-link', "onClick" => "return confirm('Are you sure you want to delete this item? This action cannot be undone.')")); ?></small>
							</h4>
							<p><small style="<?php echo ($lost->status == 0) ? 'color:rgb(240,20,20);' : 'color:rgb(20,20,240);' ; ?>"><?php echo ($lost->status == 0) ? 'Barang belum ditemukan.' : 'Barang telah ditemukan.' ; ?></small></p>
							<p><?php echo substr($lost->content, 0, 140) . ' [...]'; ?></p>
							<small><i>Posted by <?php echo ($lost->is_admin == 1) ? 'Admin' : $lost->nama; ?> on <?php echo date('D, j F Y H:i:s', strtotime($lost->date)); ?></i></small>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php unset($lost); ?>
			<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
<?php echo ($footer != NULL) ? $footer : 'footer not configured properly!'; ?>