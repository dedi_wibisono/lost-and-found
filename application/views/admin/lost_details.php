<?php echo ($header != NULL) ? $header : 'header not configured properly!'; ?>
					<li><?php echo anchor('admin', 'Found'); ?></li>
					<li class="active"><?php echo anchor('admin/lost', 'Lost'); ?></li>
					<li><?php echo anchor('admin/testimonials', 'Testimonials'); ?></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="well well-sm">
					<ul class="nav nav-pills nav-stacked">
						<li><?php echo anchor('admin/lost', 'Lost List'); ?></li>
						<li><?php echo anchor('admin/lost_add', 'Add Lost'); ?></li>
					</ul>
				</div>
				<?php echo ($sidebar != NULL) ? $sidebar : ''; ?>
			</div>
			<div class="col-md-9">
				<div class="page-header">
					<h2><?php echo $lost_content->title; ?> <small><?php echo anchor('admin/lost_edit/' . $lost_content->id, 'Edit Lost'); ?></small></h2>
				</div>
				<div class="well well-sm">
					<p><small><?php echo date('D, j F Y H:i:s', strtotime($lost_content->date)); ?></small></p>
					<p><?php echo $lost_content->content; ?></p>
					<p><small style="<?php echo ($lost_content->status == 0) ? 'color:rgb(240,20,20);' : 'color:rgb(20,20,240);' ; ?>"><?php echo ($lost_content->status == 0) ? 'Barang belum diambil.' : 'Barang telah dikembalikan.' ; ?></small></p>
				</div>
			</div>
		</div>
	</div>
<?php echo ($footer != NULL) ? $footer : 'footer not configured properly!'; ?>