<!DOCTYPE html>
<html>
<head>
	<title><?php echo $meta_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen">
	<!--[if lt IE 7]>
		<link href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css" rel="stylesheet" media="screen">
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.28468.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
	<![endif]-->
	<noscript>
		<link href="<?php echo base_url(); ?>assets/css/nojs.css" rel="stylesheet">
	</noscript>
</head>
<body>
	<div class="navbar">
		<div class="navbar-container">
			<ul class="navbar-menu">
				<?php if(!$this->session->userdata('loggedin')) : ?>
					<li><?php echo anchor('#home', '<i class="icon-home"></i> Home', array('id' => 'home')); ?></li>
				<?php else : ?>
					<li><?php echo anchor('user', '<i class="icon-home"></i> Home', array('id' => 'home')); ?></li>
				<?php endif; ?>
				<?php if(!$this->session->userdata('loggedin')) : ?>
					<li><?php echo anchor('#', '<i class="icon-user"></i> Log in', array('data-target' => '#login_modal', 'data-toggle' => 'modal')); ?></li>
				<?php endif; ?>
			</ul>
		</div>
		<div style="position:fixed;float:right;right:8px;top:5px;">
			<ul>
				<?php echo form_open('home/search'); ?>
				<li><input type="search" name="search" placeholder="Search" class="form-control" style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;"></li>
				<?php echo form_close(); ?>
			</ul>
		</div>
	</div>
	<div class="logo">
		<?php echo anchor(base_url(), '<img src="'. base_url() .'assets/img/lostandfound.png">'); ?>
	</div>
	<div class="container commentall">
		<div class="row">
			<h1 class="testi-title">All Testimonials</h1>
			<?php foreach ($comments->result() as $row) : ?>
				<div class="well well-sm">
					<h1><?php echo $row->name; ?> <small> (<?php echo $row->email; ?>) says:</small></h1>
					<p><small><i><?php echo date('D, j F Y', strtotime($row->date)); ?></i></small></p>
					<p><blockquote>"<?php echo $row->comment; ?>"</blockquote></p>
				</div>
			<?php endforeach; ?>
			<?php unset($row); ?>
			<?php echo $this->pagination->create_links(); ?>
		</div>
	</div>

	<div id="login_modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="loginlabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<?php echo form_open('home/login_post_user'); ?>
				<div class="modal-header">
					<?php echo form_button(array('class' => 'close', 'data-dismiss' => 'modal', 'content' => '<span aria-hidden="true">&times;</span>')); ?>
					<h3 class="modal-title" id="loginlabel">Login with SIAM</h3>
				</div>
				<div class="modal-body">
					<?php echo form_input(array('name' => 'nim', 'placeholder' => 'NIM', 'class' => 'form-control')); ?><br>
					<?php echo form_password(array('name' => 'password', 'placeholder' => 'Password', 'class' => 'form-control')); ?><br>
					<?php echo form_submit(array('name' => 'submit', 'value' => 'Submit', 'class' => 'btn btn-primary')); ?>
				</div>
				<div class="modal-footer">
					<small>&copy; 2014 Lost and Found</small>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>