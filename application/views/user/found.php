<?php echo ($header != NULL) ? $header : 'header not configured properly!'; ?>
					<li class="active"><?php echo anchor('user', 'Found'); ?></li>
					<li><?php echo anchor('user/lost', 'Lost'); ?></li>
					</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="well well-sm">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><?php echo anchor('user', 'Found List'); ?></li>
						<li><?php echo anchor('user/found_add', 'Add Found'); ?></li>
					</ul>
				</div>
				<?php echo ($sidebar != NULL) ? $sidebar : ''; ?>
			</div>
			<div class="col-md-9">
			<?php if($this->session->flashdata('succeess')) : ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php endif; ?>
				<div class="row">
				<?php foreach($found_list as $founded) : ?>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<img src="<?php echo base_url(); ?>assets/found/<?php echo $founded->image; ?>" alt="image">
							<div class="caption">
								<h3><?php echo $founded->title; ?></h3>
								<p><?php echo $founded->content; ?></p>
								<p><small style="<?php echo ($founded->status == 0) ? 'color:rgb(240,20,20);' : 'color:rgb(20,20,240);' ; ?>"><?php echo ($founded->status == 0) ? 'Barang belum diambil.' : 'Barang telah dikembalikan.' ; ?></small></p>
								<p><small><i>Posted by <?php echo ($founded->is_admin == 1) ? 'Admin' : $founded->nama; ?> on <?php echo date('D, j F Y H:i:s', strtotime($founded->date)); ?></i></small></p>
								<p>
									<?php echo anchor('user/found_edit/' . $founded->id, 'Edit', array('class' => 'btn btn-primary')); ?> 
									<?php echo anchor('user/found_delete/' . $founded->id, 'Delete', array('class' => 'btn btn-danger', "onClick" => "return confirm('Are you sure you want to delete this item? This action cannot be undone.')")); ?>
								</p>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<?php unset($founded); ?>
				<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
		</div>
	</div>
<?php echo ($footer != NULL) ? $footer : 'footer not configured properly!'; ?>