<!DOCTYPE html>
<html>
<head>
	<title><?php echo $meta_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen">
	<!--[if lt IE 7]>
		<link href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css" rel="stylesheet" media="screen">
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.28468.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
	<![endif]-->
	<noscript>
		<link href="<?php echo base_url(); ?>assets/css/nojs.css" rel="stylesheet">
	</noscript>
</head>
<body>
<img src="<?php echo base_url(); ?>assets/img/6.jpg" alt="0">
</body>
</html>