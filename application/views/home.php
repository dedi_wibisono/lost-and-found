<!DOCTYPE html>
<html>
<head>
	<title><?php echo $meta_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen">
	<!--[if lt IE 7]>
		<link href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css" rel="stylesheet" media="screen">
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.28468.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
	<![endif]-->
	<noscript>
		<link href="<?php echo base_url(); ?>assets/css/nojs.css" rel="stylesheet">
	</noscript>
</head>
<body>
	<div class="window">
		<div class="navbar">
			<div class="navbar-container">
				<ul class="navbar-menu">
					<?php if(!$this->session->userdata('loggedin')) : ?>
						<li><?php echo anchor('#home', '<i class="icon-home"></i> Home', array('id' => 'home')); ?></li>
					<?php else : ?>
						<li><?php echo anchor('user', '<i class="icon-home"></i> Home', array('id' => 'home')); ?></li>
					<?php endif; ?>
					<li><?php echo anchor('#about', '<i class="icon-group"></i> About Us', array('id' => 'about')); ?></li>
					<li><?php echo anchor('#found', '<i class="icon-book"></i> Our Found', array('id' => 'found')); ?></li>
					<li><?php echo anchor('#lost', '<i class="icon-search"></i> Lost', array('id' => 'lost')); ?></li>
					<li><?php echo anchor('#contact', '<i class="icon-comments"></i> Contact Us', array('id' => 'contact')); ?></li>
					<!-- <li><?php echo anchor('#login', '<i class="icon-user"></i> Log In', array('id' => 'login')); ?></li> -->					
					<?php if(!$this->session->userdata('loggedin')) : ?>
						<li><?php echo anchor('#', '<i class="icon-user"></i> Log in', array('data-target' => '#login_modal', 'data-toggle' => 'modal')); ?></li>
					<?php endif; ?>
				</ul>
			</div>
			<div style="position:fixed;float:right;right:8px;top:5px;">
				<ul>
					<?php echo form_open('home/search'); ?>
					<li><input type="search" name="search" placeholder="Search" class="form-control" style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;"></li>
					<?php echo form_close(); ?>
				</ul>
			</div>
		</div>
		<div class="logo">
			<?php echo anchor(base_url(), '<img src="'. base_url() .'assets/img/lostandfound.png">'); ?>
		</div>
		<div id="slider" class="slider">
			<img src="<?php echo base_url(); ?>assets/img/12.png" alt="0">
			<img src="<?php echo base_url(); ?>assets/img/7.jpg" alt="1">
			<img src="<?php echo base_url(); ?>assets/img/9.jpg" alt="2">
			<img src="<?php echo base_url(); ?>assets/img/8.jpg" alt="3">
			
		</div>
	</div>

	<div class="window">
		<div class="container about" id="content-about">
			<div class="row">
				<h1 class="about-title">About</h1>
				<div class="about-content">
					<p align="justify">	Lost And Found merupakan aplikasi pencari benda hilang yang memudahkan user untuk menemukan benda yang hilang dengan menginfokan pengumuman melalui Lost and Found.
						User yang menjadi target utama adalah Mahasiswa UB, karena Mahasiswa UB kerap menjadi aktor yang mengalami kehilangan benda ataupun penemuan benda.
						Karena aplikasi ini masih tahap Beta, maka jika ada masukkan silahkan <?php echo anchor('#contact', 'comment', array('class' => 'content-link contact-link')); ?> disini.
					</p>
				</div>
				<div class="about-logo" id="logo-list">
					<img src="<?php echo base_url(); ?>assets/img/ub.png">					
				</div>
				<div class="about-logo" id="logo-list-left">
						<img src="<?php echo base_url(); ?>assets/img/ptiik.png">
				</div>
			</div>
		</div>
	</div>
	<div class="window">
		<div class="container found" id="content-menu">
			<div class="row">
			<h1 class="found-title">Found <small><?php echo ($count_found > 5) ? '<small>'.anchor('home/show_all_found', 'Show All').'</small>' : '' ; ?></small></h1>
				<div id="da-slider" class="da-slider">
				<?php if($found != NULL) : ?>
				<?php foreach($found->result() as $menu) : ?>
					<div class="da-slide">
						<h2><?php echo $menu->title; ?></h2>
						<p><?php echo $menu->content; ?></p>
						<div class="da-img"><img src="<?php echo base_url(); ?>assets/found/<?php echo $menu->image; ?>" alt="image"></div>
					</div>
				<?php endforeach; ?>
				<?php unset($menu); ?>
				<?php else : ?>
					<div class="da-slide">
						<p>No product available.</p>
					</div>
				<?php endif; ?>
					<nav class="da-arrows">
						<span class="da-arrows-prev"></span>
						<span class="da-arrows-next"></span>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="window">
		<div class="container lost" id="content-lost">
			<div class="row">
				<h1 class="news-title">What's Lost? <small><?php echo ($count_lost > 3) ? '<small>'.anchor('home/show_all_lost', 'Show All', array('class' => 'content-link')).'</small>' : '' ; ?></small></h1>
				<div class="news-content">
				<?php if($lost->result() != NULL) : ?>
					<?php foreach($lost->result() as $berita) : ?>
						<div class="well well-sm">
							<h1><?php echo $berita->title; ?></h1>
							<small><i>Posted by <?php echo ($berita->is_admin == 1) ? 'Admin' : $berita->nama; ?> on <?php echo date('D, j F Y H:i:s', strtotime($berita->date)); ?></i></small>
							<p><?php echo $berita->content; ?></p>
							<p><small style="<?php echo ($berita->status == 0) ? 'color:rgb(240,20,20);' : 'color:rgb(20,20,240);' ; ?>"><?php echo ($berita->status == 0) ? 'Barang belum ditemukan.' : 'Barang telah ditemukan.' ; ?></small></p>
						</div>
					<?php endforeach; ?>
					<?php unset($berita); ?>
				<?php else : ?>
					<div class="well well-sm">
						<p>No topic yet.</p>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="window">
		<div class="container contact" id="content-contact">
			<div class="row">
				<div class="testi-form">
					<h1 class="testi-title">Tell something<br>about lost and found</h1>
					<?php echo form_open('home/testi_post'); ?>
						<?php echo form_input(array('name' => 'name', 'placeholder' => 'Name', 'class' => 'form-control')); ?><br>
						<?php echo form_input(array('name' => 'email', 'placeholder' => 'Email', 'class' => 'form-control')); ?><br>
						<?php echo form_textarea(array('name' => 'comment', 'placeholder' => 'Testimonials', 'class' => 'form-control')); ?><br>
						<?php echo form_submit(array('name' => 'submit', 'value' => 'Submit', 'class' => 'btn btn-primary')); ?>
					<?php echo form_close(); ?>
				</div>
				<div class="testi-list">
					<h1 class="testi-title">What the others say...</h1>
					<?php if($testi->result() != NULL) : ?>
					<?php foreach($testi->result() as $comment) : ?>
						<div class="well well-sm">
							<h4><?php echo $comment->name; ?> (<?php echo $comment->email; ?>) says:</h4>
							<p><small><i><?php echo date('D, j F Y', strtotime($comment->date)); ?></i></small></p>
							<p><blockquote>"<?php echo $comment->comment; ?>"</blockquote></p>
						</div>
					<?php endforeach; ?>
					<?php unset($comment); ?>
					<?php else : ?>
						<div class="well well-sm">
							<p>No testimonials yet.</p>
						</div>
					<?php endif; ?>
					<?php echo ($count_testi > 4) ? anchor('home/show_all_comments', 'Show All Comments', array('class' => 'content-link')) : '' ; ?>
				</div>
			</div>
		</div>
	</div>

	
	<div id="login_modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="loginlabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<?php echo form_open('home/login_post_user'); ?>
				<div class="modal-header">
					<?php echo form_button(array('class' => 'close', 'data-dismiss' => 'modal', 'content' => '<span aria-hidden="true">&times;</span>')); ?>
					<h3 class="modal-title" id="loginlabel">Login with SIAM</h3>
				</div>
				<div class="modal-body">
					<?php echo form_input(array('name' => 'nim', 'placeholder' => 'NIM', 'class' => 'form-control')); ?><br>
					<?php echo form_password(array('name' => 'password', 'placeholder' => 'Password', 'class' => 'form-control')); ?><br>
					<?php echo form_submit(array('name' => 'submit', 'value' => 'Submit', 'class' => 'btn btn-primary')); ?>
				</div>
				<div class="modal-footer">
					<small>&copy; 2014 Lost and Found</small>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.slides.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.cslider.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/TweenMax.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.superscrollorama.js"></script>
	<script>
		$(document).ready(function() {
			var controller = $.superscrollorama({
				reverse: true
			});
			controller.addTween("#logo-list", TweenMax.from($('#logo-list'), .6, {css:{opacity:0, right:"600px"}, ease:Quad.easeInOut}));
			controller.addTween("#logo-list-left", TweenMax.from($('#logo-list-left'), .6, {css:{opacity:0, left:"600px"}, ease:Quad.easeInOut}));
			
			$("#home").click(function() {
				$('html, body').animate({
					scrollTop: $("body").offset().top
				}, 500);
			});

			$("#about").click(function() {
				$('html, body').animate({
					scrollTop: $("#content-about").offset().top
				}, 500)
			})

			$("#found").click(function() {
				$('html, body').animate({
					scrollTop: $("#content-menu").offset().top
				}, 500);
			});

			$("#lost").click(function() {
				$('html, body').animate({
					scrollTop: $("#content-lost").offset().top
				}, 500);
			});			

			$("#contact, .contact-link").click(function() {
				$('html, body').animate({
					scrollTop: $("#content-contact").offset().top
				}, 500);
			});

			$("#login").click(function() {
				$('html, body').animate({
					scrollTop: $("#content-login").offset().top
				}, 500);
			});



			$("#slider").slidesjs({
				width: 1000,
				height: 450,
				navigation: {
					active: false,
				},
				pagination: {
					effect: "fade"
				},
				play: {
					active: false,
					effect: "fade",
					auto: true,
					interval: 4000,
					pauseOnHover: true,
					swap: false
				},
			});

			$('#da-slider').cslider({
				autoplay	: true,
				bgincrement	: 450
			});
		})
	</script>
</body>
</html>