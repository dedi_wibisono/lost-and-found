<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->model(array('found_m', 'lost_m', 'testimonials_m', 'login_user_m', 'search_m'));
	}

	function index() {
		$data['meta_title']	= 'Lost and Found in UB';
		$data['found']		= $this->found_m->get_all_founded(5, 0);
		$data['count_found']= $this->found_m->count_all_founded();
		$data['lost']		= $this->lost_m->get_all_lost(3, 0);
		$data['count_lost']	= $this->lost_m->count_total_lost();
		$data['testi']		= $this->testimonials_m->get_all_testimonials(4, 0);
		$data['count_testi']= $this->testimonials_m->count_total_testimonials();
		$this->load->view('home', $data);
	}

	function testi_post() {
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_email|required');
		$this->form_validation->set_rules('comment', 'Testimonials', 'trim|required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect(base_url() . '#contact');
		} else {
			$data = array(
				'name'		=> $this->input->post('name'),
				'email'		=> $this->input->post('email'),
				'date'		=> date('Y-m-d'),
				'comment'	=> $this->input->post('comment')
			);
			$this->testimonials_m->insert_testimonials($data);
			$this->session->set_flashdata('success', 'Post successfull.');
			redirect(base_url() . '#contact');
		}
	}

	function login_post_user() {
		$rules	= array(
			'nim'	=> array(
				'field'	=> 'nim',
				'label'	=> 'NIM',
				'rules'	=> 'trim|is_natural|required'
			),
			'password'	=> array(
				'field'	=> 'password',
				'label'	=> 'Password',
				'rules'	=> 'trim|required'
			)
		);
		$this->form_validation->set_rules($rules);

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect(base_url() . '#login');
		} else {
			$nim	= $this->input->post('nim');
			$pass	= $this->input->post('password');
			$login	= $this->login_user_m->login_user($nim, $pass);

			if(!$login) {
				$this->session->set_flashdata('error', 'Invalid username and/or password!');
				redirect('home');
			} else {
				$session	= array(
					'userid'	=> $login->id,
					'nim'		=> $login->nim,
					'nama'		=> $login->nama,
					'loggedin'	=> TRUE
				);
				$this->session->set_userdata($session);
				redirect('user');
			}
		}
	}

	function show_all_found() {
		$config['base_url']		= site_url('home/show_all_found');
		$config['total_rows']	= $this->found_m->count_all_founded();
		$config['per_page']		= 5;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);

		$data['meta_title']	= 'All Found';
		$data['found']		= $this->found_m->get_all_founded($config['per_page'], $this->uri->segment(3));
		$this->load->view('found', $data);
	}

	function show_all_lost() {
		$config['base_url']		= site_url('home/show_all_lost');
		$config['total_rows']	= $this->lost_m->count_total_lost();
		$config['per_page']		= 5;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);

		$data['meta_title']	= 'All Lost';
		$data['lost']		= $this->lost_m->get_all_lost($config['per_page'], $this->uri->segment(3));
		$this->load->view('lost', $data);
	}

	function show_all_comments() {
		$config['base_url']		= site_url('home/show_all_comments');
		$config['total_rows']	= $this->testimonials_m->count_total_testimonials();
		$config['per_page']		= 5;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);

		$data['meta_title']	= 'All Testimonials';
		$data['comments']	= $this->testimonials_m->get_all_testimonials($config['per_page'], $this->uri->segment(3));
		$this->load->view('comments', $data);
	}

	function search() {
		$this->form_validation->set_rules('search', 'Search', 'trim|alpha_numeric|xss_clean');
		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', 'Invalid search keyword (must be alphanumeric');
			redirect('home');
		} else {
			$query					= $this->input->post('search');

			// $config['base_url']		= site_url('home/search');
			// $config['total_rows']	= $this->search_m->get_search_result($query)->num_rows();
			// $config['per_page']		= 5;
			// $config['uri_segment']	= 3;
			// $this->pagination->initialize($config);

			$data['meta_title']		= 'Search Result';
			// $data['search_result']	= $this->search_m->get_search_result($query,$config['per_page'], $this->uri->segment(3));
			$data['search_result']	= $this->search_m->get_search_result($query);
			$this->load->view('search', $data);
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */