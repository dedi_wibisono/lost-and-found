<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();

		if( ! $this->session->userdata('admin_loggedin')) {
			$this->session->set_flashdata('error', 'Please login to continue.');
			redirect('auth');
		}

		$this->load->helper(array('form'));
		$this->load->library(array('form_validation', 'pagination'));
		$this->load->model(array('found_m', 'lost_m', 'testimonials_m'));
	}

	function index() {
		$config['base_url']		= site_url('admin/index');
		$config['total_rows']	= $this->found_m->count_all_founded();
		$config['per_page']		= 4;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);

		$data['meta_title']			= 'Found - Admin Panel';
		$content['header']			= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']			= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']			= $this->load->view('admin/footer', NULL, TRUE);
		$content['found_list']	= $this->found_m->get_all_founded($config['per_page'], $this->uri->segment(3));
		$this->load->view('admin/found', $content);
	}

	function found_add() {
		$data['meta_title']	= 'Add Found - Admin Panel';
		$content['header']	= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']	= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']	= $this->load->view('admin/footer', NULL, TRUE);
		$this->load->view('admin/found_add', $content);
	}

	function found_post() {
		$this->form_validation->set_rules('title', 'Found Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Found Content', 'trim|required');
		
		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('admin/found_add');
		} else {
			$config['upload_path']		= './assets/found/';
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '1024';
			$config['max_width']		= '1024';
			$config['max_height']		= '1024';
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('image')) {
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('admin/found_add');
			} else {
				$image	= $this->upload->data();
				$data 	= array(
					'title'		=> $this->input->post('title'),
					'content'	=> $this->input->post('content'),
					'image'		=> $image['file_name'],
					'imagepath'	=> $image['file_path'],
					'status'	=> $this->input->post('status'), // status awal pasti belum ditemukan
					'postedby' 	=> $this->input->post('postedby'),
					'is_admin'	=> $this->input->post('is_admin')
				);
				$this->found_m->insert_Found($data);
				$this->session->set_flashdata('success', 'Item added successfully.');
				redirect('admin');
			}
		}
	}

	function found_edit($id) {
		$data['meta_title']	= 'Edit Found - Admin Panel';
		$content['header']	= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']	= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']	= $this->load->view('admin/footer', NULL, TRUE);
		$content['found']	= $this->found_m->get_founded($id);
		$this->load->view('admin/found_edit', $content);
	}

	function found_edit_post() {
		$this->form_validation->set_rules('title', 'Found Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Found Content', 'trim|required');
		
		$id	= $this->input->post('id');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('admin/found_edit/' . $id);
		} else {
			$config['upload_path']		= './assets/found/';
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '1024';
			$config['max_width']		= '1024';
			$config['max_height']		= '1024';
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('image')) {
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('admin/found_edit/' . $id);
			} else {
				$new_img	= $this->upload->data();
				$prev_img	= $this->found_m->get_founded($id)->imagepath . $this->found_m->get_founded($id)->image;
				$data 		= array(
					'title'			=> $this->input->post('title'),
					'content'		=> $this->input->post('content'),
					'status'		=> $this->input->post('status'),
					'image'			=> $new_img['file_name'],
					'imagepath'		=> $new_img['file_path']
				);
				unlink($prev_img);
				$this->found_m->update_found($data, $id);
				$this->session->set_flashdata('success', 'Item updated successfully.');
				redirect('admin');
			}
		}
	}

	function Found_delete($id) {
		$image		= $this->found_m->get_founded($id)->imagepath . $this->found_m->get_founded($id)->image;
		unlink($image);
		$this->found_m->delete_Founded($id);
		$this->session->set_flashdata('success', 'Item deleted successfully.');
		redirect('admin');
	}

	function lost() {
		$config['base_url']		= site_url('admin/lost');
		$config['total_rows']	= $this->lost_m->count_total_lost();
		$config['per_page']		= 7;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);
		
		$data['meta_title']		= 'Lost - Admin Panel';
		$content['header']		= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']			= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']		= $this->load->view('admin/footer', NULL, TRUE);
		$content['lost_list']	= $this->lost_m->get_all_lost($config['per_page'], $this->uri->segment(3));
		$this->load->view('admin/lost', $content);
	}

	function lost_details($id) {
		$data['meta_title']			= 'Lost Details - Admin Panel';
		$content['header']			= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']			= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']			= $this->load->view('admin/footer', NULL, TRUE);
		$content['lost_content']	= $this->lost_m->get_lost($id);
		$this->load->view('admin/lost_details', $content);
	}

	function lost_add() {
		$data['meta_title']	= 'Post Lost - Admin Panel';
		$content['header']	= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']			= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']	= $this->load->view('admin/footer', NULL, TRUE);
		$this->load->view('admin/lost_add', $content);
	}

	function lost_post() {
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Content', 'trim|required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('admin/lost_add');
		} else {
			$data = array(
				'title'		=> $this->input->post('title'),
				'content'	=> $this->input->post('content'),
				'status'	=> $this->input->post('status'), // status awal pasti belum ditemukan
				'postedby' 	=> $this->input->post('postedby'),
				'is_admin'	=> $this->input->post('is_admin')
			);
			$this->lost_m->insert_lost($data);
			$this->session->set_flashdata('success', 'Item added successfully.');
			redirect('admin/lost');
		}
	}

	function lost_edit($id) {
		$data['meta_title']			= 'Edit Lost - Admin Panel';
		$content['header']			= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']			= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']			= $this->load->view('admin/footer', NULL, TRUE);
		$content['lost_content']	= $this->lost_m->get_lost($id);
		$this->load->view('admin/lost_edit', $content);
	}

	function lost_edit_post() {
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Content', 'trim|required');
		$id = $this->input->post('id');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('admin/lost_edit/' . $id);
		} else {
			$data = array(
				'title'		=> $this->input->post('title'),
				'status'	=> $this->input->post('status'),
				'content'	=> $this->input->post('content')
			);
			$this->lost_m->update_lost($data, $id);
			$this->session->set_flashdata('success', 'Item updated successfully.');
			redirect('admin/lost');
		}
	}

	function lost_delete($id) {
		$this->lost_m->delete_lost($id);
		$this->session->set_flashdata('success', 'Item deleted successfully.');
		redirect('admin/lost');
	}

	function testimonials() {
		$config['base_url']		= site_url('admin/testimonials');
		$config['total_rows']	= $this->testimonials_m->count_total_testimonials();
		$config['per_page']		= 4;
		$config['uri_segment']	= 3;
		$this->pagination->initialize($config);

		$data['meta_title']		= 'Testimonials - Admin Panel';
		$content['header']		= $this->load->view('admin/header', $data, TRUE);
		$content['sidebar']		= $this->load->view('admin/sidebar', NULL, TRUE);
		$content['footer']		= $this->load->view('admin/footer', NULL, TRUE);
		$content['testi_list']	= $this->testimonials_m->get_all_testimonials($config['per_page'], $this->uri->segment(3));
		$this->load->view('admin/testimonials', $content);
	}

	function testimonials_delete($id) {
		$this->testimonials_m->delete_testimonials($id);
		$this->session->set_flashdata('success', 'Item deleted successfully.');
		redirect('admin/testimonials');
	}

	function logout() {
		$this->session->sess_destroy();
		redirect('auth');
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */