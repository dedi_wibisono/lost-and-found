<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_m extends MY_Model {

	public function __construct() {
		parent::__construct();
	}

	function get_search_result($parms, $limit = NULL, $offset = NULL) {
		// $this->db->select('found.title AS found_title, found.content AS found_content, found.date AS found_date, found.status AS found_status, found.postedby AS found_postedby, found.is_admin AS found_is_admin,
		// 	lost.title AS lost_title, lost.content AS lost_content, lost.date AS lost_date, lost.status AS lost_status, lost.postedby AS lost_postedby, lost.is_admin AS lost_is_admin,
		// 	user.nama AS nama');
		// $this->db->from('user');
		// $this->db->join('found', 'user.id = found.postedby', 'left');
		// $this->db->join('lost', 'user.id = lost.postedby', 'left');
		// $this->db->like('found.content', $parms);
		// $this->db->or_like('lost.content', $parms);
		// return $this->db->get();

		if($limit != NULL && $offset != NULL) {
			return $this->db->query(
			"(SELECT found.id AS id, found.title AS title, found.content AS content, found.date AS date, found.status AS status, found.postedby AS postedby, found.is_admin AS is_admin, user.nama AS nama, '0' AS is_lost FROM found LEFT JOIN user ON user.id = found.postedby WHERE content LIKE '%$parms%' LIMIT $offset, $limit)
			UNION
			(SELECT lost.id AS id, lost.title AS title, lost.content AS content, lost.date AS date, lost.status AS status, lost.postedby AS postedby, lost.is_admin AS is_admin, user.nama AS nama, '1' AS is_lost FROM lost LEFT JOIN user ON user.id = lost.postedby WHERE content LIKE '%$parms%' LIMIT $offset, $limit)
			ORDER BY date DESC
			");
		} else {
			return $this->db->query(
			"(SELECT found.id AS id, found.title AS title, found.content AS content, found.date AS date, found.status AS status, found.postedby AS postedby, found.is_admin AS is_admin, user.nama AS nama, '0' AS is_lost FROM found LEFT JOIN user ON user.id = found.postedby WHERE content LIKE '%$parms%')
			UNION
			(SELECT lost.id AS id, lost.title AS title, lost.content AS content, lost.date AS date, lost.status AS status, lost.postedby AS postedby, lost.is_admin AS is_admin, user.nama AS nama, '1' AS is_lost FROM lost LEFT JOIN user ON user.id = lost.postedby WHERE content LIKE '%$parms%')
			ORDER BY date DESC
			");
		}
	}

}

/* End of file search_m.php */
/* Location: ./application/models/search_m.php */