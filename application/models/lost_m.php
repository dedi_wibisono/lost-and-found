<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lost_m extends MY_Model {

	protected $table_name	= 'lost';

	public function __construct() {
		parent::__construct();
	}

	function count_total_lost() {
		return $this->db->count_all($this->table_name);
	}

	function get_all_lost($limit = NULL, $offset = NULL, $userid = NULL) {
		$this->db->order_by('id', 'desc');
		if($userid != NULL) {
			$this->db->select('lost.id AS id, lost.title AS title, lost.content AS content, lost.date AS date, lost.status AS status, lost.postedby AS postedby, lost.is_admin AS is_admin, user.nama AS nama');
			$this->db->join('user', 'user.id = lost.postedby', 'left');
			$this->db->where('postedby', $userid);
			$this->db->limit($limit, $offset);
			$this->db->order_by('date', 'DESC');
			return $this->get();
		} elseif ($limit != NULL || $offset != NULL) {
			$this->db->select('lost.id AS id, lost.title AS title, lost.content AS content, lost.date AS date, lost.status AS status, lost.postedby AS postedby, lost.is_admin AS is_admin, user.nama AS nama');
			$this->db->from($this->table_name);
			$this->db->join('user', 'user.id = lost.postedby', 'left');
			$this->db->limit($limit, $offset);
			$this->db->order_by('date', 'DESC');
			return $this->db->get();
		} else {
			$this->db->select('lost.id AS id, lost.title AS title, lost.content AS content, lost.date AS date, lost.status AS status, lost.postedby AS postedby, lost.is_admin AS is_admin, user.nama AS nama');
			$this->db->join('user', 'user.id = lost.postedby', 'left');
			$this->db->order_by('date', 'DESC');
			return $this->get();
		}
	}

	function get_lost($id) {
		return $this->get($id);
	}

	function insert_lost($data) {
		return $this->save($data);
	}

	function update_lost($data, $id) {
		$this->save($data, $id);
	}

	function delete_lost($id) {
		$this->delete($id);
	}

}

/* End of file news_m.php */
/* Location: ./application/models/news_m.php */