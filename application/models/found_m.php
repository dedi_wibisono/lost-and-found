<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Found_m extends MY_Model {

	protected $table_name = 'found';

	public function __construct() {
		parent::__construct();
	}

	function count_all_founded($userid = NULL) {
		if($userid != NULL) {
			$this->db->where('postedby', $userid);
			return $this->db->get($this->table_name)->num_rows();
		} else {
			return $this->db->count_all($this->table_name);
		}
	}

	function get_all_founded($limit = NULL, $offset = NULL, $userid = NULL) {
		if ($userid != NULL) {
			$this->db->select('found.id AS id, found.title AS title, found.content AS content, found.image AS image, found.imagepath AS imagepath, found.date AS date, found.status AS status, found.postedby AS postedby, found.is_admin AS is_admin, user.nama AS nama');
			$this->db->join('user', 'user.id = found.postedby', 'left');
			$this->db->where('found.postedby', $userid);
			$this->db->limit($limit, $offset);
			$this->db->order_by('date', 'DESC');
			return $this->get();
		} elseif($limit != NULL || $offset != NULL) {
			$this->db->select('found.id AS id, found.title AS title, found.content AS content, found.image AS image, found.imagepath AS imagepath, found.date AS date, found.status AS status, found.postedby AS postedby, found.is_admin AS is_admin, user.nama AS nama');
			$this->db->from($this->table_name);
			$this->db->join('user', 'user.id = found.postedby', 'left');
			$this->db->limit($limit, $offset);
			$this->db->order_by('date', 'DESC');
			return $this->db->get();
		} else {
			$this->db->select('found.id AS id, found.title AS title, found.content AS content, found.date AS date, found.status AS status, found.postedby AS postedby, found.is_admin AS is_admin, user.nama AS nama');
			$this->db->join('user', 'user.id = found.postedby', 'left');
			$this->db->order_by('date', 'DESC');
			return $this->get();
		}
	}

	function get_posted() {
		$this->db->select('user.nama AS username');
		$this->db->from('user');
		$this->db->join('found', 'user.id = found.postedby');
		$this->db->get();
	}

	function get_founded($id) {
		return $this->get($id);
	}

	function insert_found($data) {
		return $this->save($data);
	}

	function update_found($data, $id) {
		$this->save($data, $id);
	}

	function delete_founded($id) {
		$this->delete($id);
	}

}

/* End of file products_m.php */
/* Location: ./application/models/products_m.php */