-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2014 at 11:30 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lostandfound`
--
CREATE DATABASE IF NOT EXISTS `lostandfound` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lostandfound`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `found`
--

CREATE TABLE IF NOT EXISTS `found` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `imagepath` varchar(300) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `postedby` int(11) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postedby` (`postedby`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `found`
--

INSERT INTO `found` (`id`, `title`, `content`, `image`, `imagepath`, `date`, `status`, `postedby`, `is_admin`) VALUES
(7, 'Dompet', 'ditemukan dompet atas nama Dedi Arief Wibisono, Fakultas PTIIK', '1581111.jpg', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-08 07:22:35', 1, 1, 1),
(8, 'hape', 'ditemukan hape', 'depan-39-13915182703.jpg', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:39:01', 0, 2, 0),
(9, 'test 1', 'asdf', 'images(2)3.jpg', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:39:50', 0, 2, 0),
(10, 'test 2', 'asdf', 'brushed11.jpg', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:40:06', 0, 2, 0),
(11, 'test 3', 'asdf', 'bios31.JPG', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:40:22', 0, 2, 0),
(12, 'test 4', 'asdf', '15811111.jpg', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:41:08', 0, 2, 0),
(13, 'test 5', 'asdf', 'bios11.JPG', 'C:/xampp/htdocs/lostandfound/assets/found/', '2014-08-12 04:41:28', 0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lost`
--

CREATE TABLE IF NOT EXISTS `lost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `postedby` int(11) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postedby` (`postedby`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `lost`
--

INSERT INTO `lost` (`id`, `title`, `content`, `date`, `status`, `postedby`, `is_admin`) VALUES
(1, 'STNK', 'telah hilang STNK atas nama Ir. Sutrisno', '2014-08-11 14:24:52', 0, 2, 0),
(8, 'hape', 'telah hilang hp samsung galaxy fame', '2014-08-13 02:56:01', 0, 1, 1),
(9, 'laptop', 'telah hilang laptop toshiba', '2014-08-13 03:27:08', 1, 2, 0),
(10, 'charger', 'telah hilang charger hp samsung', '2014-08-13 03:27:08', 0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `comment`, `email`, `date`, `name`) VALUES
(1, '>///<', 'thevirionz@yahoo.com', '2014-08-13', 'Denny'),
(2, '@.@', 'thevirionz@yahoo.com', '2014-08-13', 'Denny'),
(3, '=3=', 'thevirionz@yahoo.com', '2014-08-13', 'Denny'),
(4, '>_<', 'thevirionz@yahoo.com', '2014-08-13', 'Denny'),
(5, '^0^', 'thevirionz@yahoo.com', '2014-08-13', 'Denny'),
(6, '^3^', 'thevirionz@yahoo.com', '2014-08-13', 'Denny');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nim`, `nama`, `password`) VALUES
(2, '105060807111121', 'Dedi Arief Wibisono', 'scofield');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
